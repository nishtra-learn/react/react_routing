import kv95 from './assets/95_квартал.jpg';
import trainStation from './assets/главный_вокзал.jpg';
import quarry from './assets/карьер_ЮГОКа.jpg';
import boatStation from './assets/лодочная_станция.jpg';
import flowerClock from './assets/цветочные_часы.jpeg';


export const DATA = {
    info: {
        name: `Кривой Рог`,
        details: `Криво́й Рог (укр. Кривий Ріг) — город в Днепропетровской области Украины. Административный центр Криворожского района. До 2020 года был городом областного подчинения, который составлял Криворожский городской совет.
        Город основан в 1775 году и расположен на месте слияния рек Ингулец и Саксагань. Восьмой по численности населения город Украины, центр Криворожской агломерации. Один из самых длинных городов Европы — линейное расстояние от крайней северной точки до южной 66,1 км по прямой. На официальном сайте исполкома Криворожского городского совета указанная длина города — 126 км.        
        Кривой Рог — важный экономический и научный центр Украины, крупный транспортный узел, центр разработки Криворожского железорудного бассейна.`
    },
    mainAttraction: {
        name: `Карьер ЮГОКа`,
        src: quarry
    },
    attractions: [
        {
            name: `Карьер ЮГОКа`,
            src: quarry
        },
        {
            name: `95й Квартал`,
            src: kv95
        },
        {
            name: `Главный Вокзал`,
            src: trainStation
        },
        {
            name: `Лодочная станция`,
            src: boatStation
        },
        {
            name: `Цветочные часы`,
            src: flowerClock
        }
    ],
    photos: [
        `https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/KDPU.jpg/800px-KDPU.jpg`,
        `https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Kryvyi_Rih_National_University.JPG/800px-Kryvyi_Rih_National_University.JPG`,
        `https://upload.wikimedia.org/wikipedia/commons/c/ce/LAZ_trolleybus_at_the_station_Krivoy_Rog-Chief.jpg`,
        `https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/%D0%A1%D1%82%D0%B0%D0%BD%D1%86%D1%96%D1%8F_%D0%9A%D1%80%D0%B8%D0%B2%D0%B8%D0%B9_%D0%A0%D1%96%D0%B3-%D0%93%D0%BE%D0%BB%D0%BE%D0%B2%D0%BD%D0%B8%D0%B9_04.JPG/800px-%D0%A1%D1%82%D0%B0%D0%BD%D1%86%D1%96%D1%8F_%D0%9A%D1%80%D0%B8%D0%B2%D0%B8%D0%B9_%D0%A0%D1%96%D0%B3-%D0%93%D0%BE%D0%BB%D0%BE%D0%B2%D0%BD%D0%B8%D0%B9_04.JPG`,
        `https://upload.wikimedia.org/wikipedia/commons/5/54/Metrotram_car.jpg`,
        `https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Tram_in_Krivoy_Rog.JPG/800px-Tram_in_Krivoy_Rog.JPG`,
        `https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/%E2%84%96_137_%D0%93%D0%BE%D0%BB%D0%BE%D0%B2%D0%BF%D0%BE%D1%88%D1%82%D0%B0%D0%BC%D0%BF.jpg/800px-%E2%84%96_137_%D0%93%D0%BE%D0%BB%D0%BE%D0%B2%D0%BF%D0%BE%D1%88%D1%82%D0%B0%D0%BC%D0%BF.jpg`,
        `https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/%D0%A0%D0%B0%D0%B4%D1%83%D0%B3%D0%B0_%D0%BF%D0%BE%D1%81%D0%BB%D0%B5_%D0%B4%D0%BE%D0%B6%D0%B4%D1%8F.jpg/800px-%D0%A0%D0%B0%D0%B4%D1%83%D0%B3%D0%B0_%D0%BF%D0%BE%D1%81%D0%BB%D0%B5_%D0%B4%D0%BE%D0%B6%D0%B4%D1%8F.jpg`,
        `https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/%D0%9F%D0%B0%D1%80%D0%BA_%D1%96%D0%BC.%D0%B3%D0%B0%D0%B7%D0%B5%D1%82%D0%B8_%C2%AB%D0%9F%D1%80%D0%B0%D0%B2%D0%B4%D0%B0%C2%BB_-_%22%D0%91%D0%B5%D0%BB%D1%8B%D0%B5_%D0%B3%D0%B0%D0%B2%D0%B0%D0%BD%D0%B8%22.JPG/800px-%D0%9F%D0%B0%D1%80%D0%BA_%D1%96%D0%BC.%D0%B3%D0%B0%D0%B7%D0%B5%D1%82%D0%B8_%C2%AB%D0%9F%D1%80%D0%B0%D0%B2%D0%B4%D0%B0%C2%BB_-_%22%D0%91%D0%B5%D0%BB%D1%8B%D0%B5_%D0%B3%D0%B0%D0%B2%D0%B0%D0%BD%D0%B8%22.JPG`,
        `https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Kryvyi_Rih_-_building3.jpg/800px-Kryvyi_Rih_-_building3.jpg`
    ]
}