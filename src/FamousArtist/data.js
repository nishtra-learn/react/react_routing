import monaLisa from './assets/Mona_Lisa_by_Leonardo_da_Vinci.jpg';
import ladyWithErmine from './assets/Lady_with_an_Ermine.jpg';
import self from './assets/Leonardo_self.jpg';
import stJohnBaptist from './assets/Saint_John_the_Baptist_retouched.jpg';
import lastSupper from './assets/The_Last_Supper.jpg';
import vitruve from './assets/Vitruve_Luc_Viatour_(cropped).jpg';


export const DATA = {
    bio: `Leonardo da Vinci (English: /ˌliːəˈnɑːrdoʊ də ˈvɪntʃi, ˌliːoʊˈ-, ˌleɪoʊˈ-/; 14/15 April 1452 – 2 May 1519) was an Italian polymath of the High Renaissance who is widely considered one of the most diversely talented individuals ever to have lived. While his fame initially rested on his achievements as a painter, he also became known for his notebooks, in which he made drawings and notes on science and invention; these involve a variety of subjects including anatomy, astronomy, botany, cartography, painting, and palaeontology. Leonardo's genius epitomized the Renaissance humanist idea, and his collective works compose a contribution to later generations of artists rivalled only by that of his contemporary Michelangelo.
    Properly named Leonardo di ser Piero da Vinci, Leonardo was born out of wedlock to a notary, Piero da Vinci, and a peasant woman, Caterina, in Vinci, in the region of Florence, Italy. Leonardo was educated in the studio of the renowned Italian painter Andrea del Verrocchio. Much of his earlier working life was spent in the service of Ludovico il Moro in Milan, and he later worked in Rome, Bologna and Venice. He spent his last three years in France, where he died in 1519.
    Although he had no formal academic training, many historians and scholars regard Leonardo as the prime exemplar of the "Renaissance Man" or "Universal Genius", an individual of "unquenchable curiosity" and "feverishly inventive imagination." According to art historian Helen Gardner, the scope and depth of his interests were without precedent in recorded history, and "his mind and personality seem to us superhuman, while the man himself mysterious and remote." Scholars interpret his view of the world as being based in logic, though the empirical methods he used were unorthodox for his time.
    Leonardo is widely considered one of the greatest painters of all time. Mona Lisa is the most famous of his works and the most famous portrait ever made. The Last Supper is the most reproduced religious painting of all time and his Vitruvian Man drawing is also regarded as a cultural icon. In 2017, Salvator Mundi was sold at auction for $450.3 million, setting a new record for most expensive painting ever sold at public auction.
    Leonardo is revered for his technological ingenuity. He conceptualized flying machines, a type of armoured fighting vehicle, concentrated solar power, an adding machine, and the double hull. Relatively few of his designs were constructed or even feasible during his lifetime, as the modern scientific approaches to metallurgy and engineering were only in their infancy during the Renaissance. Some of his smaller inventions, however, entered the world of manufacturing unheralded, such as an automated bobbin winder and a machine for testing the tensile strength of wire. He is also sometimes credited with the inventions of the parachute, helicopter, and tank. He made substantial discoveries in anatomy, civil engineering, geology, optics, and hydrodynamics, but he did not publish his findings and they had little to no direct influence on subsequent science.`,
    famousPainting: {
        name: 'Mona Lisa',
        src: monaLisa,
        description: `The Mona Lisa (/ˌmoʊnə ˈliːsə/; Italian: Monna Lisa [ˈmɔnna ˈliːza] or La Gioconda [la dʒoˈkonda]; French: La Joconde [la ʒɔkɔ̃d]) is a half-length portrait painting by Italian artist Leonardo da Vinci. Considered an archetypal masterpiece of the Italian Renaissance, it has been described as "the best known, the most visited, the most written about, the most sung about, the most parodied work of art in the world". The painting's novel qualities include the subject's enigmatic expression, the monumentality of the composition, the subtle modelling of forms, and the atmospheric illusionism.
        The painting is probably of the Italian noblewoman Lisa Gherardini, the wife of Francesco del Giocondo, and is in oil on a white Lombardy poplar panel. It had been believed to have been painted between 1503 and 1506; however, Leonardo may have continued working on it as late as 1517. It was acquired by King Francis I of France and is now the property of the French Republic itself, on permanent display at the Louvre, Paris since 1797.
        The Mona Lisa is one of the most valuable paintings in the world. It holds the Guinness World Record for the highest known insurance valuation in history at US$100 million in 1962 (equivalent to $660 million in 2019).`
    },
    paintings: [
        {
            name: 'Lady with an Ermine',
            src: ladyWithErmine
        },
        {
            name: 'Self Portrait',
            src: self
        },
        {
            name: 'Saint John the Baptist',
            src: stJohnBaptist
        },
        {
            name: 'The Last Supper',
            src: lastSupper
        },
        {
            name: 'Vitruve Luc Viatour',
            src: vitruve
        }
    ]
}