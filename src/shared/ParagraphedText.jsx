import React from 'react';

export default function ParagraphedText(props) {
    let text = props.text;
    let paragraphs = text.split("\n");

    return (
        <>
            {
                paragraphs.map((p, id) => <p key={id}>{p}</p>)
            }
        </>
    )
}