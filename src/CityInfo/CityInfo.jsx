import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import ParagraphedText from '../shared/ParagraphedText';
import NavMenu from './NavMenu';
import './CityInfo.css';

export default function FamousArtist(props) {
    let data = props.data;

    return (
        <>
            <Router>
                <div className="container">
                    <NavMenu></NavMenu>
                    <Switch>
                        <Route exact path="/city/info" render={() => <CityInfo data={data.info}></CityInfo>}></Route>
                        <Route exact path="/city/main-attraction" render={() => <MainAttraction data={data.mainAttraction}></MainAttraction>}></Route>
                        <Route exact path="/city/attractions" render={() => <Attractions data={data.attractions}></Attractions>}></Route>
                        <Route path="/city/photos" render={() => <Photos data={data.photos}></Photos>}></Route>
                    </Switch>
                </div>
            </Router>
        </>
    )
}


function CityInfo(props) {
    let info = props.data;

    return (
        <>
            <h3>{info.name}</h3>
            <ParagraphedText text={info.details}></ParagraphedText>
        </>
    )
}

function MainAttraction(props) {
    let attraction = props.data;

    return (
        <div className="MainAttraction">
            <figure className="figure">
                <img src={attraction.src} className="figure-img img-fluid rounded" alt="" />
                <figcaption className="figure-caption">
                    <h3>
                        {attraction.name}
                    </h3>
                </figcaption>
            </figure>
        </div>
    )
}

function Attractions(props) {
    let attractions = props.data;

    return (
        <div className="Attractions">
            {
                attractions.map((attr, id) =>
                    <figure className="figure" key={id}>
                        <img src={attr.src} className="figure-img img-fluid rounded" alt="" />
                        <figcaption className="figure-caption">
                            {attr.name}
                        </figcaption>
                    </figure>)
            }
        </div>
    )
}

function Photos(props) {
    let photos = props.data;

    return (
        <Switch>
            <Route exact path="/city/photos" render={() =>
                <div className="Photos">
                    {
                        photos.map((photo, i) =>
                            <Link to={`/city/photos/${i + 1}`} key={i}>
                                <figure className="figure" >
                                    <img src={photo} className="figure-img img-fluid rounded" alt="" />
                                </figure>
                            </Link>)

                    }
                </div>
            }></Route>

            <Route path="/city/photos/:id(\d+)" render={(props) => {
                let id = props.match.params.id;
                if (id > 0 && id <= photos.length) {
                    let photo = photos[id - 1];

                    return (
                        <div className="Photos">
                            <figure className="figure">
                                <img src={photo} className="figure-img img-fluid rounded" alt="" />
                            </figure>)
                        </div>
                    )
                }
            }}></Route>
        </Switch>
    )
}