import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ParagraphedText from '../shared/ParagraphedText';
import NavMenu from './NavMenu';
import './FamousArtist.css';

export default function FamousArtist(props) {
    let data = props.data;
    console.log(data);

    return (
        <>
            <Router>
                <div className="container">
                    <NavMenu></NavMenu>
                    <Switch>
                        <Route exact path="/artist/bio" render={() => <Biography data={data.bio}></Biography>}></Route>
                        <Route exact path="/artist/famous-painting" render={() => <FamousPainting data={data.famousPainting}></FamousPainting>}></Route>
                        <Route path="/artist/paintings" render={() => <PaintingList data={data.paintings}></PaintingList>}></Route>
                    </Switch>
                </div>
            </Router>
        </>
    )
}


function Biography(props) {
    let text = props.data;

    return (
        <div className="Biography">
            <ParagraphedText text={text}></ParagraphedText>
        </div>
    )
}

function FamousPainting(props) {
    let painting = props.data;

    return (
        <div className="FamousPainting">
            <figure className="figure">
                <img src={painting.src} className="figure-img img-fluid rounded" alt="" />
                <figcaption className="figure-caption">
                    <h3>
                        {painting.name}
                    </h3>
                    <div className="description">
                        <ParagraphedText text={painting.description}></ParagraphedText>
                    </div>
                </figcaption>
            </figure>
        </div>
    )
}

function PaintingList(props) {
    let paintings = props.data;

    return (
        <Switch>
            <Route exact path="/artist/paintings" render={() =>
                <div className="Paintings">
                    {
                        paintings.map(art =>
                            <figure className="figure">
                                <img src={art.src} className="figure-img img-fluid rounded" alt="" />
                            </figure>
                        )
                    }
                </div>
            }></Route>
            <Route path="/artist/paintings/:id(\d+)" render={(props) => {
                let id = props.match.params.id;
                if (id > 0 && id <= paintings.length) {
                    let art = paintings[id - 1];

                    return (
                        <div className="d-flex justify-content-center">
                            <figure className="figure">
                                <img src={art.src} className="figure-img img-fluid rounded" alt="" />
                            </figure>
                        </div>
                    )
                }
            }}></Route>

        </Switch>
    )
}