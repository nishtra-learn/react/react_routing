import React from 'react';
import { NavLink } from 'react-router-dom';

export default function NavMenu() {
    return (
        <div className="ArtistNavMenu">
            <NavLink exact to="/artist/bio" className="navLink">Biography</NavLink>
            <NavLink exact to="/artist/famous-painting" className="navLink">Famous Painting</NavLink>
            <NavLink to="/artist/paintings" className="navLink">Paintings</NavLink>
        </div>

    )
}