import React from 'react';
import { NavLink } from 'react-router-dom';

export default function NavMenu() {
    return (
        <div className="CityNavMenu">
            <NavLink exact to="/city/info" className="navLink">Info</NavLink>
            <NavLink exact to="/city/main-attraction" className="navLink">Main Attraction</NavLink>
            <NavLink exact to="/city/attractions" className="navLink">Attractions</NavLink>
            <NavLink to="/city/photos" className="navLink">Photos</NavLink>
        </div>

    )
}