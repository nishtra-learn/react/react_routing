import logo from './logo.svg';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Route, Switch, NavLink } from 'react-router-dom';
import FamousArtist from './FamousArtist/FamousArtist';
import { DATA as artist_data } from './FamousArtist/data';
import CityInfo from './CityInfo/CityInfo';
import { DATA as city_data } from './CityInfo/data';

function App() {
  return (
    <div className="App">
      <Router>
        <div className="MainNav">
          <NavLink exact to="/artist/bio" className="navLink">Famous artist</NavLink>
          <NavLink exact to="/city/info" className="navLink">City</NavLink>
        </div>
        <Switch>
          <Route path="/artist/" render={() => <FamousArtist data={artist_data}></FamousArtist>}></Route>
          <Route path="/city/" render={() => <CityInfo data={city_data}></CityInfo>}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
